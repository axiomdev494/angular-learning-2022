import { Component } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { sampleData } from './sampledata';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Hello world';
  show1 = false;
  error = false;
  samplenumber = 10000000;
  sampleData = sampleData;

  changeTitle() {
    this.title = 'Something';
    console.log(this.samplenumber);
  }

  toggleShow() {
    this.show1 = !this.show1;
  }

  setError() {
    this.error = !this.error;
  }

  constructor(private fb: FormBuilder) {}

  firstName = new FormControl('Huy', [
    Validators.required,
    Validators.minLength(4),
  ]);
  lastName = new FormControl('Nguyen');

  userData = new FormGroup({
    fullName: new FormControl('', [
      Validators.required,
      Validators.maxLength(2),
    ]),
    age: new FormControl('23', [Validators.min(18)]),
    address: new FormControl(),
  });

  userData2 = new FormGroup({
    fullName: new FormControl('', [
      Validators.required,
      Validators.maxLength(2),
    ]),
    age: new FormControl('23', [Validators.min(18)]),
    address: new FormControl(),
  });

  userMobilePhone = new FormArray<any>([]);

  dummyPhonedata = [
    {
      value: '0987654321',
    },
    {
      value: '0123456789',
    },
  ];
  ngOnInit() {
    // this.firstName.valueChanges.subscribe((val) => {
    //   console.log('change: ', val);
    // });
    // this.userData.valueChanges.subscribe((val) => {
    //   console.log('change: ', val);
    // });
  }

  saveData() {
    const data = {
      firstName: this.firstName.value,
      lastName: this.lastName.value,
    };

    this.firstName.markAsTouched();

    if (this.firstName.valid) {
      console.log('Call api save');
    }

    console.log(data);
    console.log(this.firstName);
  }

  saveGroupData() {
    console.log(this.userData);
    if (this.userData.valid) {
      console.log('Call api save');
    }
  }

  setFieldValue() {
    this.firstName.setValue('Test');
    // this.firstName.disable();
  }

  setGroupValue() {
    this.userData.setValue({
      fullName: 'HuyNDH',
      age: '25',
      address: '789',
    });
  }

  saveGroupData2() {
    console.log(this.userData2);
    if (this.userData2.valid) {
      console.log('Call api save');
    }
  }

  setGroupValue2() {
    this.userData.setValue({
      fullName: 'HuyNDH',
      age: '25',
      address: '789',
    });
  }

  newForm() {
    return this.fb.group({
      phone: '',
    });
  }

  addNewNumber() {
    this.userMobilePhone.push(this.newForm());
  }

  saveFormArray() {
    console.log(this.userMobilePhone);
    console.log(this.userMobilePhone.value);
  }

  removePhone(index: number) {
    this.userMobilePhone.removeAt(index);
  }
}
